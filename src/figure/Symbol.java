package figure;

public enum Symbol {
    EMPTY('.'), BLOCK('#'), MONSTER('@'), BOMBERMAN('B');
    private char character;

    public char getValue() {
        return character;
    }

    Symbol(char symbol) {
        this.character = symbol;
    }
}
