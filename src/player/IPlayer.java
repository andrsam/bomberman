package player;

import figure.Direction;

public interface IPlayer {
    void askForMove(Direction direction);
}
