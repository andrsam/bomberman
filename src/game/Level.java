package game;

public enum Level {
    EASY(10, 3, 5), NORMAL(20, 4, 10), HARD(30, 10, 20);
    private int boardSize, blocksCount, monstersCount;

    Level(int boardSize, int blocksCount, int monstersCount) {
        this.boardSize = boardSize;
        this.blocksCount = blocksCount;
        this.monstersCount = monstersCount;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public int getBlocksCount() {
        return blocksCount;
    }

    public int getMonstersCount() {
        return monstersCount;
    }
}
