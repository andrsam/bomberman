package game;

public interface IGame {
    void start();

    void loop();

    boolean isGameContinued();

    void pause();

    void end();
}
