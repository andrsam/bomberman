package game;

import board.Board;

public class Game implements IGame {

    Level level;

    Board board;

    @Override
    public void start() {
        board = new Board(level);
        board.generate();
    }

    @Override
    public void loop() {
        while (isGameContinued()) {

        }
    }

    @Override
    public boolean isGameContinued() {
        return true;
    }


    @Override
    public void pause() {

    }

    @Override
    public void end() {

    }
}
