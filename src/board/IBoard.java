package board;

import figure.Figure;

public interface IBoard {
    void generate();

    void putFigure(Figure figure);
}
