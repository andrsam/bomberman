package board;

import figure.Direction;
import figure.Figure;
import figure.Symbol;
import game.Level;
import util.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Board implements IBoard {
    private final Random random = new Random();
    private Level level;
    private final int BOARD_SIZE = level.getBoardSize();
    private final int BLOCKS_COUNT = level.getBlocksCount();
    private final int MONSTERS_COUNT = level.getMonstersCount();
    private char[][] boardArray = new char[BOARD_SIZE][BOARD_SIZE];
    private ArrayList<Figure> monsters;
    private Figure bomberman;

    public Figure getBomberman() {
        return bomberman;
    }

    public Board(Level level) {
        this.level = level;
    }

    @Override
    public void generate() {
        initField();
        putBlocks();
        putMonsters();
        bomberman = putRandomFigure(Symbol.BOMBERMAN);
    }

    private void initField() {
        for (char[] row : boardArray) {
            Arrays.fill(row, Symbol.EMPTY.getValue());
        }
    }

    private void putBlocks() {
        for (int i = 0; i < BLOCKS_COUNT; i++) {
            int row = random.nextInt(BOARD_SIZE);
            int col = random.nextInt(BOARD_SIZE);
            boardArray[row][col] = Symbol.BLOCK.getValue();
        }
    }

    private void putMonsters() {
        for (int i = 0; i < MONSTERS_COUNT; i++) {
            Figure monster = putRandomFigure(Symbol.MONSTER);
            monsters.add(monster);
        }
    }

    private Figure putRandomFigure(Symbol symbol) {
        int row = getRandomRow();
        int col = getRandomCol(row);
        Figure figure = new Figure(row, col, symbol);
        putFigure(figure);
        return figure;
    }

    public int getRandomRow() {
        return random.nextInt(BOARD_SIZE);
    }

    public int getRandomCol(int row) {
        ArrayList<Integer> freeCells = new ArrayList<>();
        for (int col = 0; col < BOARD_SIZE; col++) {
            if (isCellFree(row, col)) {
                freeCells.add(col);
            }
        }
        return freeCells.get(random.nextInt(freeCells.size()));
    }

    @Override
    public void putFigure(Figure figure) {
        int row = figure.getRow();
        int col = figure.getCol();
        char symbol = figure.getSymbol().getValue();
        boardArray[row][col] = symbol;
    }

    public boolean isCellFree(int row, int col) {
        return (boardArray[row][col] == Symbol.EMPTY.getValue());
    }

    public void move(Figure figure, Direction direction) {
        int newRow = figure.getRow();
        int newCol = figure.getCol();
        switch (direction) {
            case UP:
                newRow--;
                break;
            case DOWN:
                newRow++;
                break;
            case LEFT:
                newCol--;
                break;
            case RIGHT:
                newCol++;
                break;
            case RANDOM:
                newCol += Utils.getRandomNumberInRange(-1, 1);
                newRow += Utils.getRandomNumberInRange(-1, 1);
                break;
        }
        if (checkCoords(newRow, newCol)) {
            figure.setRow(newRow);
            figure.setCol(newCol);
            boardArray[figure.getCol()][figure.getRow()] = Symbol.EMPTY.getValue();
            putFigure(figure);
        }
    }

    private boolean checkCoords(int row, int col) {
        boolean isCoordsInBound = (row >= 0 && row <= BOARD_SIZE - 1) && (col >= 0 && col <= BOARD_SIZE);
        boolean isCellNotBusy = boardArray[row][col] == Symbol.EMPTY.getValue();
        return isCoordsInBound && isCellNotBusy;
    }
}
